/* aisg -- parse aisg iuantAntennaBand attribute per 3gpp
 *
 * mrf (gc2majortom@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h> // int16_t
#include <limits.h> // CHAR_BIT
#include <string.h>
/*
14339->0x3803-> 0011 1000 0000 0011 ->B14,B13,B12,B5,B6
828  ->0x033c-> 0000 0011 0011 1100 ->B10,B9,B1,B2,B3,B4
0x03	15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0

Utra
EUtra	Ext	Ext	14	13	12	11	10	9	8	7	1	2	3	4	5	6
	09	08


0x08	15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0
Utra
EUtra	30	29	28	27	26	25	24	23	22	21	20	19	18	17	Res	Res


0x09	15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0
Utra
EUtra	Ext 65	32	31	44	43	42	41	40	39	38	37	36	35	34	33
	0A


0x0a	15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0
Utra
EUtra	Ext	75	74	72	71	sp	51	50	48	69	70	46	68	45	67	66
	0B

0x0b	15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0
Utra
EUtra	Res	sp	sp	sp	sp	sp	sp	sp	sp	sp	sp	sp	sp	sp	sp	76
*/
#define B6	(1 << 0)	/* 0000000000000001 */
#define B5	(1 << 1)	/* 0000000000000010 */
#define B4	(1 << 2)	/* 0000000000000100 */
#define B3	(1 << 3)	/* 0000000000001000 */
#define B2	(1 << 4)	/* 0000000000010000 */
#define B1	(1 << 5)	/* 0000000000100000 */
#define B7	(1 << 6)	/* 0000000001000000 */
#define B8	(1 << 7)	/* 0000000010000000 */
#define B9	(1 << 8)	/* 0000000100000000 */
#define B10	(1 << 9)	/* 0000001000000000 */
#define B11	(1 << 10)	/* 0000010000000000 */
#define B12	(1 << 11)	/* 0000100000000000 */
#define B13	(1 << 12)	/* 0001000000000000 */
#define B14	(1 << 13)	/* 0010000000000000 */
#define EXT08	(1 << 14)	/* 0100000000000000 */
#define EXT09	(1 << 15)	/* 1000000000000000 */

char *usage =
	"aisg iuantantennaband parser\n"
	"usage: aisg 14339\n"
	"or in hex\n"
	"usage: aisg 0x3803\n";

void binprintf(int v)
{
	unsigned int mask = 1 << (sizeof (u_int16_t) * CHAR_BIT - 1);
	while(mask) {
        printf("%d", (v&mask ? 1 : 0));
        mask >>= 1;
    }
}

int bit_test(int bit, u_int16_t word)
{
    bit = 1 << bit;
    return(bit & word);
}

int main(int argc, char *argv[])
{
    if (!argv[1] || strcmp(argv[1], "-help") == 0)
    {
	printf("%s", usage);
	return -1;
    }

    u_int16_t iuant,band;
    int i;
    iuant = strtoul(argv[1], NULL, 0);
    band = iuant;
    printf("\n iuantAntennaOperatingBand:\n dec:\t%d\n hex:\t%#04x\n bin:\t",\
	   iuant,band);
    binprintf(band);
    printf("\n EUTRA Bands: ");

    printf("\n 0x03\t15\t14\t13\t12\t11\t10\t9\t8\t7\t6\t5\t4\t3\t2\t1\t0\n");
    for(i=15; i>=0; i--) {
	   switch(bit_test(i,band)) {
		   case B6:
			   printf("\tB6");
			   break;
		   case B5:
			   printf("\tB5");
			   break;
		   case B4:
			   printf("\tB4");
			   break;
		   case B3:
			   printf("\tB3");
			   break;
		   case B2:
			   printf("\tB2");
			   break;
		   case B1:
			   printf("\tB1");
			   break;
		   case B7:
			   printf("\tB7");
			   break;
		   case B8:
			   printf("\tB8");
			   break;
		   case B9:
			   printf("\tB9");
			   break;
		   case B10:
			   printf("\tB10");
			   break;
		   case B11:
			   printf("\tB11");
			   break;
		   case B12:
			   printf("\tB12");
			   break;
		   case B13:
			   printf("\tB13");
			   break;
		   case B14:
			   printf("\tB14");
			   break;
		   case EXT08:
			   printf("\tEXT08");
			   break;
		   case EXT09:
			   printf("\tEXT09");
			   break;
		   default:
			   printf("\t");
	   }
    }
    printf("\n");
    return 0;
}
