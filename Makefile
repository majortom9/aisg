CC=gcc
CFLG= -g -Wall
SRC=aisg.c
OBJ=aisg.o 
BIND=/usr/local/bin/

TARGET=aisg
all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(CFLG) $(OBJ) -o $(TARGET) $(CLIB) -lm

$(OBJ): $(HED)

install: all
	cp $(TARGET) $(BIND)

uninstall: 
	rm $(BIND)$(TARGET)

clean:
	rm -f $(OBJ1) $(TARGET) *.o *~ ._*

%.o: %.c
	$(CC) $(CFLG) $(INCLUDE) -c $< -o $@

